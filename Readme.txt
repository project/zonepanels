ZonePanels

Description
--------------------------
ZonePanels allow you can easy create dynamic layout for Panels, best to use with Bootstrap 3 theme.
ZonePanels allow you customize 6*12=72 regions for your layout.

Installation
--------------------------
1. Copy the entire fft directory the Drupal sites/all/modules
directory or use Drush with drush dl zonepanels.
2. Login as an administrator. Enable the module on the Modules page.

Usage
--------------------------
- Create your Panels page or edit exist Panels page.
- In the Layout section choose ZONES Category then choose ZONESTRAP layout.
- ZonePanels allow you customize for 6 Zone, each zone support config for 12 region.
- After config region, click "Update and Save", with each region choose Style with ZoneStrap then you can choose column
  style for your region, column style based on Bootstrap grid column.