<?php

/**
 * @file
 * Main implement for ZonesPanels
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function zonepanels_ctools_plugin_directory($owner, $plugin_type) {
  switch ("$owner:$plugin_type") {
    case 'panels:layouts':
    case 'panels:styles':
      return "plugins/$plugin_type";
  }

  return NULL;
}

/**
 * Default config for Zone Panels.
 */
function zonepanels_default_panels_config() {
  $zonepanels_default = array(
    'rows' => 6,
    'cols' => 12,
    'wrapper' => array(
      'prefix' => '',
      'suffix' => '',
    ),
    'zone_1'  => array('zone' => 1),
    'zone_2'  => array('zone' => 1),
    'zone_3'  => array('zone' => 1),
    'zone_4'  => array('zone' => 0),
    'zone_5'  => array('zone' => 0),
    'zone_6'  => array('zone' => 0),
  );

  return $zonepanels_default;
}


/**
 * Implementation of hook_form_ctools_export_ui_edit_item_wizard_form_alter().
 */
function zonepanels_form_ctools_export_ui_edit_item_wizard_form_alter(&$form, &$form_state, $form_id) {
  zonepanels_form_panels_panel_context_edit_content_alter($form, $form_state, $form_id);
}

/**
 * Implements hook_form_panels_panel_context_edit_content_alter().
 */
function zonepanels_form_panels_panel_context_edit_content_alter(&$form, &$form_state, $form_id) {
  if(!isset($form_state['layout']) || $form_state['layout']['module'] != 'zonepanels') return;

  $layout_prefix      = @$form_state['display']->layout_settings['zonepanels']['wrapper']['prefix'] ?: "";
  $layout_suffix      = @$form_state['display']->layout_settings['zonepanels']['wrapper']['suffix'] ?: "";
  $zonepanels_default = zonepanels_default_panels_config();
  $zone_list          = range(0, $zonepanels_default['cols']);
  $zonepanels         = @$form_state['display']->layout_settings['zonepanels'] ?: $zonepanels_default;

  $form['layout_settings']['zonepanels'] = array(
    '#type'        => 'fieldset',
    '#tree'        => TRUE,
    '#title'       => t('ZONES'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );

  $form['layout_settings']['zonepanels']['wrapper'] = array(
    '#type'        => 'fieldset',
    '#tree'        => TRUE,
    '#title'       => t('Wrapper'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );

  $form['layout_settings']['zonepanels']['wrapper']['prefix'] = array(
    '#type'          => 'textfield',
    '#required'      => FALSE,
    '#title'         => t('Prefix'),
    '#default_value' => $layout_prefix,
  );

  $form['layout_settings']['zonepanels']['wrapper']['suffix'] = array(
    '#type'          => 'textfield',
    '#required'      => FALSE,
    '#title'         => t('Suffix'),
    '#default_value' => $layout_suffix,
  );

  for ($i = 1; $i <= $zonepanels_default['rows']; $i++) {
    $form['layout_settings']['zonepanels']['zone_' . $i] = array(
      '#type'        => 'fieldset',
      '#tree'        => TRUE,
      '#title'       => t('ZONE ' . $i),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
      '#attributes' => array(
        'class' => array("zone-group-{$i}"),
      ),
    );

    $form['layout_settings']['zonepanels']['zone_' . $i]['zone'] = array(
      '#type'          => 'select',
      '#required'      => TRUE,
      '#title'         => t('Region for ZONE-' . $i),
      //'#description'   => t('Long description'),
      '#default_value' => $zonepanels['zone_' . $i]['zone'],
      '#empty_option'  => t('- Select -'),
      '#options'       => $zone_list,
      '#attributes' => array(
        'class' => array("zone-region-{$i}"),
      ),
    );

    $form['layout_settings']['zonepanels']['zone_' . $i]['wrapper'] = array(
      '#type'        => 'fieldset',
      '#tree'        => TRUE,
      '#title'       => t('Wrapper'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );

    $form['layout_settings']['zonepanels']['zone_' . $i]['wrapper']['prefix'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Prefix'),
      '#default_value' => @$zonepanels['zone_' . $i]['wrapper']['prefix'] ?: "",
    );

    $form['layout_settings']['zonepanels']['zone_' . $i]['wrapper']['suffix'] = array(
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#title'         => t('Suffix'),
      '#default_value' => @$zonepanels['zone_' . $i]['wrapper']['suffix'] ?: "",
    );
  }
}